package com.vxcompany.meetup.tripservice.trip;

import com.vxcompany.meetup.tripservice.exception.UserNotLoggedInException;
import com.vxcompany.meetup.tripservice.user.User;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class TripServiceTest {

    private static final User GUEST = null;
    private static final User REGISTERED_USER = UserBuilder.builder().build();
    private static final User UNKNOWN_USER = UserBuilder.builder().build();
    private static final Trip TO_BAARN = new Trip();

    @InjectMocks
    private TripService tripService;

    @Mock
    private TripDAO tripDAO;

    @Test
    void should_throw_exception_when_user_is_not_logged_in() {
        assertThrows(
                UserNotLoggedInException.class,
                () -> tripService.getTripsByUser(UNKNOWN_USER, GUEST)
        );
    }

    @Test
    void should_return_nothing_when_other_is_not_a_friend() {
        final List<Trip> tripsByUser = tripService.getTripsByUser(UNKNOWN_USER, REGISTERED_USER);

        assertTrue(tripsByUser.isEmpty());
    }

    @Test
    void should_return_trips_from_befriended_user() {
        final User befriendedUser = UserBuilder.builder()
                .withFriends(REGISTERED_USER)
                .build();

        when(tripDAO.tripsBy(befriendedUser))
                .thenReturn(Collections.singletonList(TO_BAARN));

        final List<Trip> tripsByFriend = tripService.getTripsByUser(befriendedUser, REGISTERED_USER);

        assertEquals(1, tripsByFriend.size());
    }

    private static class UserBuilder {
        private User[] friends = new User[]{};
        private Trip[] trips = new Trip[]{};

        public static UserBuilder builder() {
            return new UserBuilder();
        }

        public UserBuilder withFriends(final User... friends) {
            this.friends = friends;
            return this;
        }

        public UserBuilder withTrips(final Trip... trips) {
            this.trips = trips;
            return this;
        }

        public User build() {
            final User user = new User();
            Arrays.asList(friends).forEach(user::addFriend);
            Arrays.asList(trips).forEach(user::addTrip);
            return user;
        }

        private UserBuilder() {
        }
    }
}