package com.vxcompany.meetup.tripservice.trip;

import com.vxcompany.meetup.tripservice.exception.UserNotLoggedInException;
import com.vxcompany.meetup.tripservice.user.User;

import java.util.ArrayList;
import java.util.List;

public class TripService {
    private final TripDAO tripDAO;

    public TripService(final TripDAO tripDAO) {
        this.tripDAO = tripDAO;
    }

    public List<Trip> getTripsByUser(final User requestedUser, final User loggedInUser) throws UserNotLoggedInException {
        if (loggedInUser == null) {
            throw new UserNotLoggedInException();
        }

        return requestedUser.isFriendsWith(loggedInUser)
                ? tripDAO.tripsBy(requestedUser)
                : new ArrayList<>();
    }
}
